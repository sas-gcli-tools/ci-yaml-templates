# CI YAML Templates

Contains a yaml file for the pipelines in all the repos in this project.

You can include them, something like this.
``` yaml
include:
  - 'https://gitlab.com/sas-gcli-tools/ci-yaml-templates/raw/master/name_of_template.yml'
```

Note - depending on template, it will probably require at least a vars.yml file and may impose some other requirements on the repository.
